# Сгенерировать фреймы (трубется sh):

```
./generate_frames_from_video.sh sample.mp4 frames

```
Это создаст BMP файлы в каталоге frames, которые позже целиком будут загружены в память видео-сервера.

# Запустить видео-сервер

```
video_stream.exe
```

# Запустить http сервер (требуется Python)

```
./http_server.sh
``` 

# Открыть http://localhost:8080
