#include "common.hpp"
#include "frame_source.hpp"
#include "muxer.hpp"
#include "encoder.hpp"
#include "ws_server.hpp"

#include <boost/asio.hpp>
#include <map>

namespace conf
{
  int const width = 1280;
  int const height = 720;

  char const* frames_dir = "frames";
  unsigned const frames_count_max = 10000;

  char const* h264_dll_path = "third_party/TTH264/bin/TTH264.dll";

  const int port = 8086;
  unsigned const fps = 25;
}

int main()
{
  using namespace vs;

  try
  {
    init_ffmpeg();

    H264Lib::dll_path(conf::h264_dll_path);

    boost::asio::io_service io_service;

    using MuxerMap = std::map<
      WsServer::ClientId,
      std::shared_ptr<Muxer>,
      std::owner_less<WsServer::ClientId>>;

    MuxerMap muxers;
    Encoder encoder(conf::width, conf::height, conf::fps);

    auto on_frame_changed = [&](Frame const& frame, unsigned time)
    {
      if (muxers.empty() && !encoder.nal_units().empty())
        return;

      VS_PRINT("encoding:" << frame.name());

      unsigned const stride_size = frame.width() * 4;
      encoder.encode(time, frame.rgba_data(), stride_size, stride_size * frame.height());

      replace_start_codes(encoder.m_compressed.begin(), encoder.m_compressed.end());
      for (auto& pair : muxers)
        pair.second->write_frame_packet(encoder.m_compressed.data(), encoder.m_compressed.size());
    };

    auto on_client_connect = [&](WsServer::ClientId id, BufferWriter sender) {
      VS_INFO("client connected");
      muxers[id] = std::make_shared<Muxer>(sender, encoder.nal_units(), conf::width, conf::height, conf::fps);
      VS_INFO("clients count:" << muxers.size());
    };

    auto on_client_disconnect = [&](WsServer::ClientId id) {
      muxers.erase(id);
      VS_INFO("client disconnected, count:" << muxers.size());
    };

    FrameSource frames(
      io_service,
      conf::frames_dir,
      conf::frames_count_max,
      conf::width,
      conf::height,
      conf::fps,
      on_frame_changed);

    WsServer server(
      io_service,
      conf::port,
      on_client_connect,
      on_client_disconnect);

    io_service.run();
  }
  catch (std::exception const& err)
  {
    std::cerr << "EXCEPTION: " << err.what() << std::endl;
  }
  
  std::cin.get();

  return 0;
}
