#pragma once

#include "common.hpp"

#include <boost/noncopyable.hpp>
#include <boost/filesystem.hpp>

#include <memory>
#include <fstream>
#include <vector>
#include <set>
#include <string>

namespace vs
{
  struct Frame
    : boost::noncopyable
  {
    Frame(std::string const& bmp_path)
      : m_width()
      , m_height()
      , m_name(bmp_path)
    {
      std::ifstream file(bmp_path.c_str(), std::ios::binary);
      VS_ASSERT(file);

      char header[54];
      file.read(header, 54);

      m_width = *(int*)&header[18];
      m_height = *(int*)&header[22];

      unsigned const stride_size = sizeof(int32_t) * m_width;

      m_rgba_data = std::make_unique<char[]>(stride_size * m_height);

      int cur_row = m_height;
      while (cur_row-- > 0)
        file.read(m_rgba_data.get() + stride_size * cur_row, stride_size);

      // convert to BGRA
      for (int idx = 0; idx < m_width * m_height; idx++)
        std::swap(
          m_rgba_data[idx * sizeof(int32_t) + 0],
          m_rgba_data[idx * sizeof(int32_t) + 2]);
    }

    unsigned char* rgba_data() const { return reinterpret_cast<unsigned char*>(m_rgba_data.get()); }
    int width() const { return m_width;  }
    int height() const { return m_height; }
    std::string const& name() const { return m_name;  }

  private:
    int m_width;
    int m_height;
    std::string const m_name;
    std::unique_ptr<char[]> m_rgba_data;
  };


  std::vector<std::shared_ptr<Frame>> read_frames(std::string const& frames_dir, unsigned frames_count_max)
  {
    using dir_iter = boost::filesystem::directory_iterator;

    std::set<std::string> file_names;

    for (
      dir_iter iter(frames_dir);
      (iter != dir_iter()) && frames_count_max--;
      ++iter)
      file_names.insert(iter->path().string());

    std::vector<std::shared_ptr<Frame>> frames;

    VS_INFO("preparing " << file_names.size() << " frames...");

    for (auto& name : file_names)
    {
      VS_PRINT("preparing:" << name);
      frames.push_back(std::make_shared<Frame>(name));
    }
    VS_PRINT_END();

    return frames;
  }
}
