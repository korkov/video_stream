#pragma once

#include "common.hpp"
#include <h264_base.h>

#include <boost/function.hpp>
#include <boost/dll.hpp>

#include <vector>
#include <algorithm>

namespace vs
{
  namespace encoder_detail
  {
    using namespace h264;
    using h264::HANDLE;

    struct H264Lib
    {
      static std::string const& dll_path(std::string const& path = "")
      {
        static std::string saved;
        if (!path.empty())
          saved = path;
        return saved;
      }

      static boost::dll::shared_library& instance()
      {
        VS_ASSERT(!dll_path().empty());
        static boost::dll::shared_library lib(dll_path());
        return lib;
      }
    };

    auto const& create_f()
    {
      static auto const func =
        H264Lib::instance().get<HANDLE(const CreationContext&)>(TTH264CreateEntryPoint);
      return func;
    }

    auto const& encode_f()
    {
      static auto const func =
        H264Lib::instance().get<bool(HANDLE, uint, const byte* const, uint, uint)>(TTH264EncodeEntryPoint);
      return func;
    }

    auto const& destroy_f()
    {
      static auto const func =
        H264Lib::instance().get<void(HANDLE)>(TTH264DestroyEntryPoint);
      return func;
    }

    typedef std::vector<byte> buffer;

    CreationContext make_context(buffer& compressed, int width, int height, int fps)
    {
      struct Mess
        : public MessageStream
      {
      public:
        Mess(char const* type)
          : m_type(type) {}
        void OnMessage(const char* const szMsg)
        {
          VS_INFO("H264: " << m_type << ": " << szMsg);
        }
        char const* m_type;
      };

      class Writer
        : public IOWriteStream
      {
      public:
        Writer(buffer& compressed)
          : compressed(compressed)
        {}

        void Write(const byte* const rgBytes, uint uiSizeInBytes)
        {
          compressed.resize(uiSizeInBytes);
          memcpy((void*)&compressed.front(), rgBytes, uiSizeInBytes);
        }

        buffer& compressed;
      };

      CreationContext ctx;
      ctx.m_pMsgStream = new Mess("MSG");
      ctx.m_pWrnStream = new Mess("WRN");
      ctx.m_pErrStream = new Mess("ERR");

      ctx.m_pWriter = new Writer(compressed);

      ctx.m_uiWidth = width;
      ctx.m_uiHeight = height;
      ctx.m_uiFrameRate = fps;
      ctx.m_uiTargetBitRate = 4 * 1000000;
      //ctx.m_eType = ET_SW;
      ctx.m_eType = ET_HW;
      ctx.m_eIFFormat = IFF_BGRA;

      return ctx;
    }

    typedef unsigned char Byte;

    struct NalUnits
    {
      std::vector<Byte> sps;
      std::vector<Byte> pps;

      bool empty() const { return sps.empty() || pps.empty(); }
    };

    NalUnits extract_nal_units(std::vector<Byte> const& compressed)
    {
      static std::vector<Byte> const nal = { 0, 0, 0, 1 };

      VS_ASSERT(compressed.size() > nal.size());

      VS_ASSERT(compressed[nal.size()] == 103);

      auto sps_end = std::search(compressed.begin() + nal.size(),
        compressed.end(), nal.begin(), nal.end());
      VS_ASSERT(sps_end != compressed.end());

      VS_ASSERT(*(sps_end + nal.size()) == 104);

      auto pps_end = std::search(sps_end + nal.size(),
        compressed.end(), nal.begin(), nal.end());
      VS_ASSERT(pps_end != compressed.end());

      NalUnits result;

      result.sps.assign(compressed.begin(), sps_end);
      result.pps.assign(sps_end, pps_end);

      VS_INFO("sps size:" << result.sps.size() << ", pps size:" << result.pps.size());

      return result;
    }

    struct Encoder
      : boost::noncopyable
    {
      Encoder(int width, int height, int fps)
      {
        m_context = create_f()(make_context(m_compressed, width, height, fps));
      }

      void encode(uint time, const byte* const bytes, uint stride_size, uint size)
      {
        encode_f()(m_context, time, bytes, stride_size, size);

        if (m_nalu.empty())
          m_nalu = extract_nal_units(m_compressed);
      }

      NalUnits const& nal_units()
      {
        return m_nalu;
      }

      ~Encoder()
      {
        destroy_f()(m_context);
      }

      buffer m_compressed;
      HANDLE m_context;
      NalUnits m_nalu;
    };
  }

  using encoder_detail::Encoder;
  using encoder_detail::NalUnits;
  using encoder_detail::H264Lib;
}
