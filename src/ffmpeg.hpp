#pragma once

#include <stdint.h>
#include <boost/cstdint.hpp>

//#undef snprintf
//#define snprintf _snprintf
#define PRId64 "llu"

extern "C"
{
#include <libavutil/avassert.h>
#include <libavutil/channel_layout.h>
#include <libavutil/opt.h>
#include <libavutil/mathematics.h>
#include <libavutil/timestamp.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
}
