#pragma once

#include <iostream>
#include <assert.h>
#include <stdexcept>
#include <string>
#include <sstream>

#ifdef NDEBUG
#  define VS_ASSERT(WHAT) do{ \
  if (!(WHAT)) \
    throw std::logic_error( \
      std::string("ASSERTION: "#WHAT", "__FILE__", line: ") + \
      std::to_string(__LINE__)); \
  } while(0)
#else
#  define VS_ASSERT assert
#endif

namespace vs
{
  enum class LogLevel { quiet, error, warning, info };
  LogLevel const log_level = LogLevel::info;
}

#define VS_LEVEL_MESSAGE(LEVEL, WHAT)                   \
do {                                                    \
  using namespace vs;                                   \
  if (log_level <= LogLevel::LEVEL)                     \
  {                                                     \
    std::stringstream ss;                               \
    std::cerr << #LEVEL": " << WHAT << std::endl;       \
  }                                                     \
} while (0)

#define VS_PRINT(WHAT)                                           \
do {                                                             \
  std::stringstream ss;                                          \
  std::cerr << '\r' << WHAT << "                " << std::flush; \
} while (0)

#define VS_PRINT_END()                                           \
do {                                                             \
  std::cerr << std::endl;                                        \
} while (0)


#define VS_INFO(WHAT) VS_LEVEL_MESSAGE(info, WHAT)
#define VS_WARN(WHAT) VS_LEVEL_MESSAGE(warning, WHAT)
#define VS_ERROR(WHAT) VS_LEVEL_MESSAGE(error, WHAT)
