#pragma once

#include "read_frames.hpp"

#include <boost/noncopyable.hpp>
#include <boost/date_time.hpp>
#include <boost/signals2.hpp>
#include <boost/asio.hpp>

#include <string>
#include <functional>

namespace vs
{
  struct FrameSource
    : boost::noncopyable
  {
    FrameSource(
      boost::asio::io_service& io_service,
      std::string const& path,
      unsigned frames_count_max,
      unsigned width,
      unsigned height,
      unsigned fps,
      std::function<void(Frame const&, unsigned)> on_changed)
      : m_frames(read_frames(path, frames_count_max))
      , m_interval(1000 / fps)
      , m_timer(io_service, m_interval)
      , m_current_frame_number(0)
      , m_current_time(0)
      , m_on_changed(on_changed)
    {
      for (auto& frame: m_frames)
        if (frame->width() != width || frame->height() != height)
          throw std::runtime_error("invalid frame: " + frame->name());

      start_timer();
    }

  private:
    void start_timer()
    {
      m_timer.async_wait([this](const boost::system::error_code&) {
        next_frame();
      });
    }

    void next_frame()
    {
      m_timer.expires_at(m_timer.expires_at() + m_interval);
      start_timer();

      m_on_changed(*m_frames[m_current_frame_number], m_current_time);

      ++m_current_frame_number %= m_frames.size();
      m_current_time += 40;
    }

    std::vector<std::shared_ptr<Frame>> m_frames;
    boost::posix_time::milliseconds m_interval;
    boost::asio::deadline_timer m_timer;
    unsigned m_current_frame_number;
    unsigned m_current_time;
    std::function<void(Frame const&, unsigned)> m_on_changed;
  };
}