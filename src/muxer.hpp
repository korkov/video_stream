#pragma once

#include "ffmpeg.hpp"
#include "common.hpp"
#include "encoder.hpp"

// for htons
#include <boost/asio.hpp>

#include <algorithm>
#include <vector>

namespace vs
{
  typedef unsigned char Byte;

  template <typename Iterator>
  bool is_start_code(Iterator it)
  {
    return (*(it) == 0) &&
      (*(it + 1) == 0) &&
      (*(it + 2) == 0) &&
      (*(it + 3) == 1);
  };


  template <typename Iterator>
  void replace_start_codes(Iterator const begin, Iterator const end)
  {
    auto it = begin;
    auto start_code = end;

    while (it != end)
    {
      if (is_start_code(it))
      {
        if (start_code != end)
        {
          size_t ss = htonl(std::distance(start_code, it) - 4);
          memcpy(&(*start_code), &ss, 4);
        }

        start_code = it;
        it = it + 4;
      }
      else
      {
        it++;
      }
    }

    if (start_code != end)
    {
      size_t ss = htonl(std::distance(start_code, it) - 4);
      memcpy(&(*start_code), &ss, 4);
    }
  }


  static void ffmpeg_log(void*, int, const char* format, va_list argptr)
  {
    char buf[1024];
    va_start(argptr, format);
    int ret = _vsnprintf(buf, sizeof(buf), format, argptr);
    if (ret == -1)
    {
      VS_WARN("ffmpeg_log cannot create message");
      return;
    }
    buf[sizeof(buf) - 1] = 0;
    VS_INFO("ffmpeg: " << buf);
  }

  static uint8_t* alloc_extradata(
    unsigned char const* sps, size_t sps_size,
    unsigned char const* pps, size_t pps_size,
    int* extradata_size)
  {
    size_t const avcc_extra_size = 11 + AV_INPUT_BUFFER_PADDING_SIZE;
    size_t const data_size = sps_size + pps_size + avcc_extra_size;

    uint8_t* data = (uint8_t*)av_malloc(data_size);
    memset(data, 0, data_size);

    uint8_t* p = data;

    *p = 1; p++;
    *p = sps[1]; p++;
    *p = sps[2]; p++;
    *p = sps[3]; p++;
    *p = 0xFC | 3; p++;
    *p = 0xE0 | 1; p++;

    uint16_t sps_size16 = htons(uint16_t(sps_size));
    memcpy(p, &sps_size16, 2);
    p = p + 2;

    for (size_t i = 0; i < sps_size; ++i)
    {
      *p = sps[i]; p++;
    }

    *p = 1; p++;


    uint16_t pps_size16 = htons(u_short(pps_size));
    memcpy(p, &pps_size16, 2);
    p = p + 2;

    for (size_t i = 0; i < pps_size; ++i)
    {
      *p = pps[i]; p++;
    }

    *extradata_size = (p - data);

    assert((*extradata_size + AV_INPUT_BUFFER_PADDING_SIZE) <= int(data_size));

    return (uint8_t *)data;
  }


  static void init_ffmpeg()
  {
    avcodec_register_all();
    av_register_all();
    av_log_set_level(AV_LOG_WARNING);
    av_log_set_callback(ffmpeg_log);
  };

  using BufferWriter = std::function<void(char const*, size_t)>;

  class Muxer
  {
  public:
    Muxer(BufferWriter sender, NalUnits const& nalu, int width, int height, int fps)
      : m_sender(sender)
      , m_nalu(nalu)
      , m_fmt_ctx()
      , m_pts(0)
      , m_width(width)
      , m_height(height)
      , m_fps(fps)
    {
      VS_ASSERT(!m_nalu.sps.empty());
      VS_ASSERT(!m_nalu.pps.empty());

      m_fmt_ctx = avformat_alloc_context();
      m_fmt_ctx->oformat = av_guess_format("mp4", NULL, NULL);

      VS_ASSERT(m_fmt_ctx->oformat);

      if (!m_fmt_ctx->oformat)
        throw std::runtime_error("cannot gues foarmt mp4");

      {
        uint8_t *avio_ctx_buffer = NULL;
        size_t avio_ctx_buffer_size = 1024 * 50;

        VS_INFO("AVIOContext buffer size:" << avio_ctx_buffer_size);

        avio_ctx_buffer = (uint8_t *)av_malloc(avio_ctx_buffer_size);
        VS_ASSERT(avio_ctx_buffer);

        if (avio_ctx_buffer == NULL)
          throw std::runtime_error("cannot allocate avio_ctx_buffer");

        AVIOContext *avio_ctx = avio_alloc_context(avio_ctx_buffer, avio_ctx_buffer_size,
          AVIO_FLAG_WRITE, this, NULL, wss_write, NULL);
        VS_ASSERT(avio_ctx);

        m_fmt_ctx->pb = avio_ctx;
        m_fmt_ctx->flags |= AVFMT_FLAG_BITEXACT;
      }


      AVOutputFormat* oformat = m_fmt_ctx->oformat;
      oformat->video_codec = AV_CODEC_ID_H264;
      oformat->audio_codec = AV_CODEC_ID_NONE;

      AVCodec* codec = avcodec_find_encoder(oformat->video_codec);
      VS_ASSERT(codec);

      AVStream* stream = avformat_new_stream(m_fmt_ctx, codec);
      VS_ASSERT(m_fmt_ctx->nb_streams == 1);
      assert(stream->id == (m_fmt_ctx->nb_streams - 1));

      AVCodecParameters *codecpar = stream->codecpar;
      codecpar->codec_type = AVMEDIA_TYPE_VIDEO;
      codecpar->codec_id = oformat->video_codec;
      codecpar->width = m_width;
      codecpar->height = m_height;
      codecpar->format = AV_PIX_FMT_YUV420P;
      codecpar->profile = m_nalu.sps[1];
      codecpar->level = m_nalu.sps[3];

      VS_INFO("codec profile: " << codecpar->profile << ", " << "level: " << codecpar->level);

      codecpar->extradata = alloc_extradata(
        m_nalu.sps.data(), m_nalu.sps.size(),
        m_nalu.pps.data(), m_nalu.pps.size(),
        &codecpar->extradata_size);

      if (m_fmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
        m_fmt_ctx->flags |= CODEC_FLAG_GLOBAL_HEADER;

      AVDictionary* opt = NULL;
      av_dict_set(&opt, "movflags", "+faststart+empty_moov+default_base_moof+frag_custom", 0);
      avformat_write_header(m_fmt_ctx, &opt);
    }

    void write_frame_packet(Byte* data, size_t size)
    {
      AVPacket pkt;
      av_init_packet(&pkt);

      pkt.buf = 0;
      pkt.data = data;
      pkt.size = size;

      pkt.stream_index = 0;

      if ((int(data[4]) == 0x65) || (int(data[4]) == 0x67))
      {
        pkt.flags |= AV_PKT_FLAG_KEY;
      }

      size_t const fake_fps = m_fps * 2; // magic for desktop

      m_pts += av_rescale_q(1, av_make_q(1, fake_fps), m_fmt_ctx->streams[0]->time_base);

      pkt.pts = m_pts;
      pkt.dts = m_pts;

      av_write_frame(m_fmt_ctx, &pkt);
      av_write_frame(m_fmt_ctx, NULL);
    }

    ~Muxer()
    {
      int const ret = av_write_trailer(m_fmt_ctx);
      assert(ret == 0);
      avformat_free_context(m_fmt_ctx); // flush, close pb?
    }

    static int wss_write(void *opaque, uint8_t *data, int size)
    {
      Muxer* muxer = reinterpret_cast<Muxer*>(opaque);
      muxer->m_sender((char const*)data, size_t(size));
      return size;
    }

    BufferWriter m_sender;
    NalUnits const& m_nalu;
    AVFormatContext *m_fmt_ctx;
    int64_t m_pts;
    int const m_width;
    int const m_height;
    int const m_fps;
  };
}
