#pragma once

#include "common.hpp"

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

#include <functional>
#include <stdexcept>

namespace vs
{
  struct WsServer
  {
    using Server = websocketpp::server<websocketpp::config::asio>;
    using ClientId = websocketpp::connection_hdl;
    using BufferWriter = std::function<void(char const*, size_t)>;

    WsServer(boost::asio::io_service& io_service, int port,
      std::function<void(ClientId, BufferWriter)> on_connect,
      std::function<void(ClientId)> on_disconnect)
      : m_on_connect(on_connect)
      , m_on_disconnect(on_disconnect)
    {
      m_serv.clear_access_channels(websocketpp::log::alevel::all);
      m_serv.init_asio(&io_service);

      m_serv.set_open_handler([this](ClientId id) {
        auto sender = [=](char const* data, size_t size)
        {
          VS_ASSERT(id.use_count() != 0);

          try
          {
            m_serv.send(id, data, size, websocketpp::frame::opcode::binary);
          }
          catch (std::exception const& err)
          {
            VS_WARN("ws_server: cannot send to client:" << err.what());
          }
        };

        try
        {
          m_on_connect(id, sender);
        }
        catch (std::exception const& err)
        {
          VS_ERROR("ws_server: connection hanlder:" << err.what());
        }
      });

      m_serv.set_close_handler([this](ClientId id) {
        m_on_disconnect(id);
      });

      VS_INFO("ws_server: listening port: " << port);
      m_serv.listen(port);
      m_serv.start_accept();
    }

  private:
    Server m_serv;

    std::function<void(ClientId, BufferWriter)> m_on_connect;
    std::function<void(ClientId)> m_on_disconnect;
  };
}
