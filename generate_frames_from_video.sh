#!/bin/sh

[ -z "$2" ] && { echo "Usage: $0 VIDEO_FILE OUTDIR" 1>&2; exit 2; }
mkdir $2 || exit 1

./ffmpeg -i $1 \
       -vf "drawtext=fontsize=20:fontfile=./subfont.ttf:text=$2/'%{expr_int_format\\:n\\:d\\:6}.bmp':x=200:y=0:box=1:boxcolor=white" \
       -pix_fmt rgb32 \
       -start_number 0 \
       -f image2 \
       $2/%06d.bmp
