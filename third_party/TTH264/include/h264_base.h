#ifndef H264_H
#define H264_H
  
#define TTH264CreateEntryPoint      "TTH264Create"
#define TTH264EncodeEntryPoint      "TTH264Encode"
#define TTH264DestroyEntryPoint     "TTH264Destroy"

typedef unsigned int  uint;
typedef unsigned char byte;

#ifdef _MSC_VER
  #if ( _MSC_VER <= 1500 )
    #define nullptr 0
  #endif
#endif

  namespace h264
  {
    struct  EncoderHandle;
    typedef EncoderHandle* HANDLE;
//////////////////////////////////////////////////////////////////////////
    enum EncoderType
    {
      ET_HW = 0,  // use hardware encoder if available, if not return false
      ET_SW,      // use software encoder if available, if not return false
      ET_AUTO     // use hardware encoder if available, if not use software encoder
    }; // enum EncoderType
//////////////////////////////////////////////////////////////////////////
    enum InputFrameFormat
    {
      IFF_BGRA = 0,
      IFF_UNKNOWN,
    }; // enum InputFrameFormat
//////////////////////////////////////////////////////////////////////////
    class MessageStream
    {
    public:
      virtual void OnMessage( const char* const szMsg ) = 0;
    }; // class MessageStream
//////////////////////////////////////////////////////////////////////////
    class IOWriteStream
    {
    public:
      virtual void Write( const byte* const rgBytes, uint uiSizeInBytes ) = 0;
    }; // class IOWriteStream
//////////////////////////////////////////////////////////////////////////
    struct CreationContext
    {
      MessageStream*    m_pMsgStream;        // can be null
      MessageStream*    m_pWrnStream;        // can be null
      MessageStream*    m_pErrStream;        // can not be null
      IOWriteStream*    m_pWriter;           // can not be null
      uint              m_uiWidth;           // width of input picture  
      uint              m_uiHeight;          // height of input picture  
      uint              m_uiFrameRate;       // FPS
      uint              m_uiTargetBitRate;   // target bit rate, only valid for the software encoder
      EncoderType       m_eType;             // encoder type
      InputFrameFormat  m_eIFFormat;         // input picture/frame format

      CreationContext()
        : m_pMsgStream( nullptr )
        , m_pWrnStream( nullptr )
        , m_pErrStream( nullptr )
        , m_pWriter( nullptr )
        , m_uiTargetBitRate( 0 )
        , m_eType( ET_AUTO ) 
        , m_eIFFormat( IFF_BGRA )
      {}
    }; // struct CreationContext 
  }; // namespace h264
//////////////////////////////////////////////////////////////////////////
  typedef h264::HANDLE (*TTH264CreateFPtr) ( const h264::CreationContext& ctx );
  typedef bool         (*TTH264EncodeFPtr) ( h264::HANDLE pEncoder, uint uiTime, const byte* const rgBytes, uint uiRowStrideInBytes, uint uiSize );
  typedef void         (*TTH264DestroyFPtr)( h264::HANDLE pEncoder );
#endif //  H264_H